import Navbar from './Navbar';
import './App.css';
import Login from './Login';
import InputItems from './InputItems';
import CreateAccount from './CreateAccount';
import AccountsList from './AccountsList';
import Home from './Home';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

function App() {
  return (
    <Router>
      <div className="App">
        <Navbar />
        <Home />
        <div className="content">
          <Switch>
            <Route exact path="/login">
              <Login />
            </Route>
            <Route exact path="/input">
              <InputItems />
            </Route>
            <Route exact path="/create-account">
              <CreateAccount />
            </Route>
            <Route exact path="/accounts-list">
              <AccountsList />
            </Route>
          </Switch>
        </div>
      </div>
    </Router>
  );
}

export default App;
