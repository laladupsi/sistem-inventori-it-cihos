import { Link } from 'react-router-dom';
import logo from './assets/logo.png';
import './Navbar.css';

const Navbar = () => {
    return ( 
        <nav className="navbar">
            <div className="logo">
                <img src={logo} alt=""/>
            </div>
            <div className="links">
                <Link to="/input">Input Items</Link>
                <Link to="/create-account">Create New Account</Link>
                <Link to="/accounts-list">Accounts List</Link>
                <Link to="/login">Login</Link>
            </div>
        </nav>
    );
}
 
export default Navbar;