import './Home.css';
import Button from '@mui/material/Button';

const Home = () => {
    return ( 
        <div className='home'>
            <div className='title'>
                <p>Sistem Inventori IT</p>
            </div>
            <div className='view-button'>
                <Button variant="contained" onClick={() => {
                    alert('clicked');
                }}
                >LIHAT ITEM</Button>
            </div>
        </div>
            
        
    );
}
 
export default Home;